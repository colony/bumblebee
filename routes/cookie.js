var express = require('express');
var router = express.Router();
var http = require('http');
var util = require('util');

var fs = require("fs");
var file = "/usr/local/bumblebee/test.db";
var exists = fs.existsSync(file);

if(!exists) {
  console.log("Creating DB file.");
  fs.openSync(file, "w");
}

var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(file);

db.serialize(function() {
 //create table if not exist
    db.run("CREATE TABLE IF NOT EXISTS COOKIE_TEXT" 
     + "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
     + "body TEXT,"
        + "create_time INTEGER"
        + ")"
     );
});

router.post('/createtext', function(req, res){
    var text = req.query.text;
    var now = new Date().getTime();
     db.run("insert into COOKIE_TEXT(body, create_time) values(?, ?)", [text, now]);
     res.writeHead(200)
     res.end();
});

router.get('/getnewesttexts', function(req, res){
 var textId = req.query.text_id;
 var limit = req.query.limit;
 // console.log(textId);
 // console.log(limit);
 db.all('select * from COOKIE_TEXT where id<? order by id desc limit ?', [textId, limit],
 function (err, rows){
     if (err){
            console.err(err);
            res.status(500);
        }else {
            // console.log(rows);
         res.json({ "data" : rows});
     }
 });
});
// db.serialize(function() {
// 	//create table if not exist
//     db.run("CREATE TABLE IF NOT EXISTS COOKIE_TEXT" 
//     	+ "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
//     	+ "body TEXT,"
//         + "create_time INTEGER,"
//         + "num_votes INTEGER"
//         + ")"
//     	);

//     db.run("CREATE TABLE IF NOT EXISTS USER"
//         + "(user_id TEXT,"
//         + "vote_status INTEGER,"
//         + "text_id INTEGER,"
//         + "FOREIGN KEY(text_id) REFERENCES COOKIE_TEXT(id)"
//         + ")"
//         );

// });

// router.post('/createtext', function(req, res){
//     var text = req.query.text;
// 	var now = new Date().getTime();
// 	db.run("insert into COOKIE_TEXT(body, create_time, num_votes) values(?, ?, 0)", [text, now]);
// 	res.writeHead(200)
// 	res.end();
// });

// router.get('/getnewesttexts', function(req, res){
// 	var textId = req.query.text_id;
// 	var limit = req.query.limit;
// 	db.all('select * from COOKIE_TEXT inner join USER on COOKIE_TEXT.id=USER.text_id where COOKIE_TEXT.id<? order by COOKIE_TEXT.id desc limit ?', [textId, limit],
// 	function (err, rows){
// 		if (err){
//             console.err(err);
//             res.status(500);
//         }else {
// 			res.json({ "data" : rows});
// 		}
// 	});
// });

// router.get('/getratedtexts', function(req, res){
//     var num_votes = req.query.num_votes;
//     var limit = req.query.limit;
//     db.all('select * from COOKIE_TEXT left join USER on COOKIE_TEXT.id=USER.text_id where COOKIE_TEXT.num_votes<? order by COOKIE_TEXT.num_votes desc limit ?', [num_votes, limit],
//     function (err, rows){
//         if (err){
//             console.err(err);
//             res.status(500);
//         }else {
//             res.json({ "data" : rows});
//         }
//     });
// });
	

// router.post('/upvote', function(req, res){
// 	var textId = req.query.text_id;
//     var userId = req.query.user_id;
//     var num_votes = req.query.num_votes;
//     console.log(userId);
//     console.log(num_votes);
//     console.log(textId);
//     if(num_votes == 1)
//         db.run("insert into USER(user_id, vote_status, text_id) values(?, 1, ?)", [userId, textId]);
// 	else if(num_votes == 2)
//         db.run("update USER set vote_status=1 where user_id=? and text_id=?", [userId, textId]);

//     db.run("update COOKIE_TEXT set num_votes=num_votes + ? where id=?", [num_votes, textId], function(err, row){
// 		if (err){
//             console.err(err);
//             res.status(500);
//         }
//         else {
//             res.status(202);
//         }
//         res.end();
// 	});
// });

// router.post("/downvote", function(req, res){
// 	var textId = req.query.text_id;
//     var userId = req.query.user_id;
//     var num_votes = req.query.num_votes;

//     if(num_votes == -1)
//         db.run("insert into USER(user_id, vote_status, text_id) values(?, -1, ?)", [userId, textId]);
//     else if(num_votes == -2)
//         db.run("update USER set vote_status=-1 where user_id=? and text_id=?", [userId, textId])

// 	db.run("update COOKIE_TEXT set num_votes=num_votes + ? where id=?", [num_votes, textId], function(err, row){
// 		if (err){
//             console.err(err);
//             res.status(500);
//         }
//         else {
//             res.status(202);
//         }
//         res.end();
// 	});
// });

module.exports = router;