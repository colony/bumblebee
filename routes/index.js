var express = require('express');
var router = express.Router();
var fs = require('fs');
var http = require('http');
var util = require('util');




router.get('/', function(req, res) {
	res.render('index', { title: 'Ninjr' });
});

router.get('/privacy', function(req, res) {
    handleReqAndRes(req, res, 'privacy');
});

router.get('/terms', function(req, res) {
    handleReqAndRes(req, res, 'terms');
});

router.get('/aboutus', function(req, res) {
    handleReqAndRes(req, res, 'aboutus');
});

router.get('/code', function(req, res) {
    handleReqAndRes(req, res, 'code');
});

router.get('/ninjrcookie', function(req, res){
	res.render("ninjrcookie", { title: 'Ninjr' });
});

router.get('/posts/:token*', function(req, res, next){
	//todo change to decode token
	//YWR8MQ== 
	//1YZWR8MQ== -> ad|1
	//1YZWR8Mg== -> ad|2
	//1YZWR8Mw== -> ad|3
    //1YZWR8NA== -> ad|4
    //1YZWR8NQ== -> ad|5
	var token = req.param('token');

	if(token.substring(0,1) == 1){
		token = token.substring(1, token.length); // delete 1
		token = token.substring(0,1) + token.substring(2, token.length); // delete second digit
		if(token.length % 4 == 1) token = token + "=";   
		if(token.length % 4 == 2) token = token + "==";
		if(token.length % 4 == 3) token = token + "=";
		var decodeString = decode64(token).split("|");
		var origin = decodeString[0];
		var id = decodeString[1];
		if(id == parseInt(id)){
			var post;
			fs.readFile("json/"+id+".json", "utf8", function(err, data){
				if(err) {
					var options = {
					  host: "server.ninjrapp.com",
					  path: '/gd/v1/posts/'+ id + '/web',
					  port: '8080',
					  method: 'GET',
					  headers: {'Authorization': 'Bearer 83df9cdc8b8fa04a91beaf93c8c4a53e', 
					  			'Accept': 'application/json',
					  			'X-GD-CLIENT' : 'WEB:4',
					  			'X-GD-LOCALE' : 'en_US',
					  			'X-GD-GEO' : '1|1.0|1.0'
					  			}
					};
					var request = http.get(options, function(resp){
				  	 // console.log('STATUS: ' + resp.statusCode);

					  var data = '';
				  	  resp.setEncoding('utf8');
				  	  resp.on('data', function (chunk) {
				  	  	data += chunk;
				  	  });
				  	  resp.on('end', function(){
				  	  	post = JSON.parse(data);
				  	  //	console.log(util.inspect(post.comments.data[3], {showHidden: false, depth: null}));
				  	  	res.render("post", {post : post, title: "ninjr"});
				  	  });
					});

					request.on('error', function(e) {
					  console.log('problem with request: ' + e.message);
					});
					// write data to request body
					request.end();
					
				}else{
					post = JSON.parse(data);
					res.render("post", {post : post, title: "ninjr"});
				}
				
			});
		}else{
			 res.status(404).send('404: Page not Found');
		}
	}else{
		res.status(404).send('404: Page not Found');
	}
});

router.get("/blogger/:id*", function(req, res){
	var id = req.param('id');;
	fs.readFile("json/"+id+".json", "utf8", function(err, data){
		var post = JSON.parse(data);
		res.render("blogger", {post: post, title: "ninjr blogger"});
	});
});


router.post('/saveemail', function(req, res){
    var email = req.query.email;
	fs.appendFile('email.txt', email+"\n", function (err) {

	});
	console.log(req.query.email);
	res.writeHead(200)
	res.end();

});


function handleReqAndRes(req, res, fileName){
	var ua = req.headers['user-agent'];
	if(isMobile(ua)){
		// if(isMobileSafari(ua)){
		// 	res.writeHead(301,
		// 	  {Location: 'https://itunes.apple.com/us/app/id968448971'}
		// 	);
		// 	res.end();
		// }else{
			if(fileName != "aboutus"){
				res.render(fileName+"Mobile", { title: 'Ninjr' });
			}else{
				res.render(fileName, { title: 'Ninjr' });
			}
		// }
	}else{
		res.render(fileName, { title: 'Ninjr' });
	}
}

function isMobile(ua){
	return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(ua);
}

function isMobileSafari(ua) {
    return ua.match(/(iPod|iPhone|iPad)/) && ua.match(/AppleWebKit/)
}

function encode64(token){
    var result = new Buffer(token);
    return result.toString("base64");
}

function decode64(token){
	var result = new Buffer(token, 'base64').toString('ascii');
	return result;

}

module.exports = router;
