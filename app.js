var express = require('express');
var engine = require('ejs-locals');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var vhost = require('vhost');
var routes = require('./routes/index');
var cookie = require('./routes/cookie');

// var aboutus = require('./routes/aboutus');
// var privacy = require('./routes/privacy');
// var terms = require('./routes/terms');
// var code = require('./routes/code');

var app = express();
app.use(express.static(path.join(__dirname, 'public')));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('ejs', engine);
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

//
app.use(vhost('ninjrcookie.com', function(req, res, next){
  res.redirect(301, 'http://ninjrapp.com/ninjrcookie')
}));

app.use('/', routes);
app.use('/', cookie);
// app.use('/aboutus', aboutus);
// app.use('/privacy', privacy);
// app.use('/terms', terms);
// app.use('/code', code);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

if (app.get('env') === 'production') {
  require('newrelic');
}

module.exports = app;
